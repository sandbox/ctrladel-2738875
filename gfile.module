<?php
/**
 * @file
 * Contains file entity management functionality for the Group module.
 */

/**
 * Implements hook_entity_info_alter().
 */
function gfile_entity_info_alter(&$entity_info) {
  // Users can only have one group parent.
  $entity_info['file']['group entity'] = 'multiple';
}

/**
 * Control access to a file.
 *
 * Modules may implement this hook if they want to have a say in whether or not
 * a given user has access to perform a given operation on a file.
 *
 * The administrative account (user ID #1) always passes any access check,
 * so this hook is not called in that case. Users with the "bypass file access"
 * permission may always view and edit files through the administrative
 * interface.
 *
 * Note that not all modules will want to influence access on all
 * file types. If your module does not want to actively grant or
 * block access, return FILE_ENTITY_ACCESS_IGNORE or simply return nothing.
 * Blindly returning FALSE will break other file access modules.
 *
 * @param string $op
 *   The operation to be performed. Possible values:
 *   - "create"
 *   - "delete"
 *   - "update"
 *   - "view"
 *   - "download"
 * @param object $file
 *   The file on which the operation is to be performed, or, if it does
 *   not yet exist, the type of file to be created.
 * @param object $account
 *   A user object representing the user for whom the operation is to be
 *   performed.
 *
 * @return string|NULL
 *   FILE_ENTITY_ACCESS_ALLOW if the operation is to be allowed;
 *   FILE_ENTITY_ACCESS_DENY if the operation is to be denied;
 *   FILE_ENTITY_ACCESS_IGNORE to not affect this operation at all.
 *
 * @ingroup file_entity_access
 * @see file_entity_access()
 */
function gfile_file_entity_access($op, $file, $account) {
  $access = FALSE;

  if($op == 'create') {
    $memberships = group_membership_load_by_user($account->uid);

    $access =  user_access('bypass group access', $account);

    foreach($memberships as $membership) {
      $group = group_load($membership->gid);
      $access = group_access('create files', $group, $account);

      if($access) {
        break;
      }
    }

    if($access) {
      return FILE_ENTITY_ACCESS_ALLOW;
    }
    else {
      return FILE_ENTITY_ACCESS_IGNORE;
    }
  }

  if(is_object($file) && isset($file->group)) {
    $type = $file->type;
    $own_file = $account->uid == $file->uid;

    $access =  user_access('bypass group access', $account);

    switch($op){
      case 'view':
        foreach($file->group as $gid) {

          if($access) {
            break;
          }

          $group = group_load($gid);
          if(group_access('view files', $group, $account) ||
            (group_access('view own files', $group, $account) && $own_file)
          ) {
            $access = TRUE;
          }
        }
        break;
      case 'update':
        foreach($file->group as $gid) {

          if($access) {
            break;
          }

          $group = group_load($gid);
          if (group_access('edit any ' . $type . ' files', $group, $account) ||
             (group_access('edit own ' . $type . ' files', $group, $account) && $own_file)
          ) {
            $access = TRUE;
          }
        }
        break;
      case 'delete':
        foreach($file->group as $gid) {

          if($access) {
            break;
          }

          $group = group_load($gid);
          if (group_access('delete any ' . $type . ' files', $group, $account) ||
             (group_access('delete own ' . $type . ' files', $group, $account) && $own_file)
          ) {
            $access = TRUE;
          }
        }
        break;
      case 'download':
        foreach($file->group as $gid) {

          if($access) {
            break;
          }

          $group = group_load($gid);
          if (group_access('download any ' . $type . ' files', $group, $account) ||
             (group_access('download own ' . $type . ' files', $group, $account) && $own_file)
          ) {
            $access = TRUE;
          }
        }
        break;
    }

    if($access) {
      return FILE_ENTITY_ACCESS_ALLOW;
    }
    else {
      return FILE_ENTITY_ACCESS_DENY;
    }
  }

  return FILE_ENTITY_ACCESS_IGNORE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function gfile_form_file_entity_add_upload_alter(&$form, &$form_state, $form_id) {
  if($form['#step'] == 4 && $form_state['storage']['scheme'] == 'private') {
    $file = $form['#entity'];
    $can_bypass_access = user_access('bypass group access');
    $global_create = user_access('create files');

    $form_state['#group']['entity'] =& $form['#entity'];
    $form_state['#group']['entity_type'] = 'file';
    $form_state['#group']['global_create'] = $global_create;

    // Add a vertical tab for group selection.
    $form['group_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Group settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#group' => 'additional_settings',
      '#attributes' => array(
        'class' => array('file-form-group-information'),
      ),
      '#attached' => array(
        'js' => array(drupal_get_path('module', 'gfile') . '/misc/gfile.js'),
      ),
      '#tree' => TRUE,
      '#weight' => 10,
      '#element_validate' => array('group_entity_group_settings_validate'),
    );

    $form['group_settings']['existing_gids'] = array(
      '#type' => 'value',
      '#value' => [],
      '#parent' => array('group_settings'),
    );

    // Find out which form API element we should be using.
    $element_type = $can_bypass_access
      ? variable_get('group_entity_choice_admin_element', 'autocomplete')
      : variable_get('group_entity_choice_standard_element', 'select');

    // Unset the fid so the following functions properly identify this as a new
    // file. This is okay because this is a copy of the file that is only
    // passed to the one function below.
    unset($file->fid);

    // Use a helper function to generate the form element.
    $element_function = "group_entity_group_settings_{$element_type}_element";
    $element = function_exists($element_function) ? $element_function('file', $file, "create files", $global_create) : array();

    if (empty($element) || !is_array($element)) {
      // The user is not able to change the group for this entity so don't show
      // the form controls.
      $form['group_settings']['#access'] = FALSE;
      return;
    }

    // Add the group select form element to the form.
    $form['group_settings'] += $element;

    if (isset($form['#submit'])) {
      array_unshift($form['#submit'], 'group_entity_group_settings_submit', 'gfile_file_entity_add_upload_submit');
    }
    else {
      $form['#submit'] = ['group_entity_group_settings_submit', 'gfile_file_entity_add_upload_submit'];
    }

    form_load_include($form_state, 'inc', 'gfile', 'gfile');
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function gfile_form_file_entity_edit_alter(&$form, &$form_state, $form_id) {
  global $user;

  $file = $form_state['file'];

  $groups = group_entity_parent_gids($file);
  $gids = array_keys($groups);

  $can_bypass_access = user_access('bypass group access');
  $global_create = user_access('create files');

  $form_state['#group']['entity'] =&  $form_state['file'];
  $form_state['#group']['entity_type'] = 'file';
  $form_state['#group']['global_create'] = $global_create;

  // Add a vertical tab for group selection.
  $form['group_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Group settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'additional_settings',
    '#attributes' => array(
      'class' => array('file-form-group-information'),
    ),
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'gfile') . '/misc/gfile.js'),
    ),
    '#tree' => TRUE,
    '#weight' => -50,
    '#element_validate' => array('group_entity_group_settings_validate'),
  );

  $form['group_settings']['existing_gids'] = array(
    '#type' => 'value',
    '#value' => $gids,
    '#parent' => array('group_settings'),
  );

  // Find out which form API element we should be using.
  $element_type = $can_bypass_access
    ? variable_get('group_entity_choice_admin_element', 'auto_complete')
    : variable_get('group_entity_choice_standard_element', 'select');

  // Use a helper function to generate the form element.
  $element_function = "group_entity_group_settings_{$element_type}_element";
  $element = function_exists($element_function) ? $element_function('file', $file, "create files", $global_create) : array();

  // If the user is not able to change the group for this entity, we do not show
  // the group selection vertical tab.
  if (empty($element) || !is_array($element)) {
    $form['group_settings']['#access'] = FALSE;
    return;
  }

  // Add the group select form element to the form.
  $form['group_settings'] += $element;

  array_unshift($form['actions']['submit']['#submit'], 'group_entity_group_settings_submit');
  form_load_include($form_state, 'inc', 'gfile', 'gfile');
}

/**
 * Submit function to set the parent groups on file entities.
 *
 * @see gfile_form_file_entity_add_upload_alter()
 */
function gfile_file_entity_add_upload_submit($form, &$form_state) {
  if (!empty($form_state['values']['group_settings'])) {
    $form_state['values']['group'] = array_keys($form_state['#group']['entity']->group);
  }
  else {
    $form_state['values']['group'] = [];
  }
}
