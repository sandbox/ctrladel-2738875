<?php
/**
 * @file
 * Hook implementations for the Group module.
 */

/**
 * Implements hook_group_permission().
 */
function gfile_group_permission() {
  // Copy of permissions from file_entity.
  $permissions = array(
    'administer files' => array(
      'title' => t('Administer files'),
      'restrict access' => TRUE,
    ),
    'create files' => array(
      'title' => t('Add and upload new files'),
    ),
    'view own files' => array(
      'title' => t('View own files'),
    ),
    'view files' => array(
      'title' => t('View files'),
    ),
  );

  // Generate standard file permissions for all applicable file types.
  foreach (file_entity_permissions_get_configured_types() as $type) {
    $permissions += file_entity_list_permissions($type);
  }

  return $permissions;
}
